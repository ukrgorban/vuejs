Vue.component('anime', {
    props: {
        data:{
            type: Object,
        }
    },
    template: `
        <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="img">
                    <img v-bind:src="data.image_url" v-bind:alt="data.title">
                </div>
                <div class="card-body">
                    <p class="card-text">{{ data.title }}</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a target="_blank" v-bind:href="data.url">Перейти</a>
                        </div>
                        <small class="text-muted">Епизодов {{ data.episodes }}</small>
                        <small>Тип: {{ data.type }}</small>
                    </div>
                </div>
            </div>
        </div>
    `
});


