Vue.component('preloader', {
    template: `
        <div class="spinner-border" role="status">
            <img src="img/45.gif" alt="loader">
        </div>
    `
});