let app = new Vue({
    el: '#app',
    data:{
        load: false,
        animes: [],
        stor: []
    },

    created: function () {

        setTimeout(() => {

            this.getData();
            this.load = true;

        }, 2000);

    },

    computed:{

        first10: function(){

            this.animes = this.stor.slice(0, 10);

            return this.animes;

        },

        reverseData: function(){

            this.animes = this.stor.reverse();

            return this.animes;

        },

        typeTv: function(){

            this.animes = this.stor.filter( anime => anime.type == 'TV');

            return this.animes;

        },

        typeMovie: function(){

           this.animes = this.stor.filter( anime => anime.type == 'Movie');

            return this.animes;

        }

    },

    methods:{

        getData: function(){

            fetch('https://api.jikan.moe/v3/top/anime')
                .then(res => res.json())
                .then(res => {
                    this.stor = res.top;
                    this.animes = res.top;
                })
                .catch(err => console.log(err));

        },

        test: function(){

            fetch('https://api.jikan.moe/v3/top/anime')
                .then(res => res.json())
                .then(res => console.log(res.top))
                .catch(err => console.log(err));

        },

        removeData: function(){

            this.animes = [];

        },

        getFirst10: function(){

            this.animes = this.first10;

        },

        getDataReverse: function(){

            this.animes = this.reverseData;

        },

        getTypeMovie: function(){

            this.animes = this.typeMovie;

        },

        getTypeTv: function(){

            this.animes = this.typeTv;

        }

    }
});